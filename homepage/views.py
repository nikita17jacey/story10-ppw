from django.shortcuts import render, redirect
from .forms import RegisForm
from django.contrib import messages

# Create your views here.
def Index(request):
    return render(request, 'story10.html')

def regis(request):
    if request.method =="POST":
        form = RegisForm(request.POST)
        if form.is_valid():
            form.save()
            name = form.cleaned_data.get('first_name')
            messages.success(request, f'Congrats! your account was successfully created')
            return redirect('login')
    else:
        form = RegisForm()
        dict ={
        'form':form,
    }
    return render(request, 'regis.html', dict)

